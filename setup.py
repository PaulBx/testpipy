from setuptools import setup

setup(
    name='TestSimpleCalculatorBx',
    version='0.0.1',
    author="Paul Bouyssoux",
    packages=['Package_Calculator'],
    description="TestSimpleCalculator is a simple package \
    in order to make some test on packaging principles in Python",
    license='GNU GPLv3',
    python_requires ='>=3.4',
)

