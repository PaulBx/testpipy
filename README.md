# Exercice 10 (pypi)

## Objectif
L'objectif de cet exercice est d'importer les classes contenues dans le package Package_Calculator, grâce à test Pypi, permettant ainsi d'importer les modules automatiquement, sans modifier le PYTHONPATH à chaque fois qu'on souhait utiliser les fonctions du repo qui communiquent avec un autre package.

## Méthode
Pour cela, on récupère la même organisation des deux exercices précédents.  
On créer un compte sur le site Test.Pypi.org, puis on créér un token (TestSimpleCalculatorBx)  
On créé un fichier python setup.py permettant de référencer le nom du token et le nom du package qu'il représente.  
Ensuite on upload le token et on installe le package.  
On peut donc ensuite se servir des classes présentes dans le package.

## Installation du package
Dans un premier temps, il faut installer des modules pour l'importation et l'upload du package:

    python3 -m pip install --user --upgrade setuptools wheel

    python3 setup.py sdist bdist_wheel
    
puis une fois le token créé sur test.Pypi on peut entrer les commandes permettant l'upload :

    python3 -m pip install --user --upgrade twine
    
    python3 -m twine upload --repository testpypi dist/*
    
Il faut à présent installer le package :

    sudo apt-get install python3-venv
    python3 -m venv venv #crétation d'un virtual env (venv)
    source ./venv/bin/activate
    
    pip install -i https://test.pypi.org/simple/ TestSimpleCalculatorBx #(prendre la commande donnée sur le site test.pytp)
    
Le programme est ainsi fonctionnel

## Ressources
https://packaging.python.org/tutorials/packaging-projects/#packaging-your-project